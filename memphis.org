#+TITLE:  Mapping Memphis Opportunity
#+AUTHOR: Dylan Ledbetter
#+EMAIL:  dtledbetter@gmail.com
#+DATE:   2016 Apr 04
#+TAGS:   memphis opportunity geography data explorer civic tech

* .idea
  https://www.ipums.org/
  
* partners
  https://www.tn.gov/transparenttn
  [[http://growth-engine.org/][EDGE]]
 
* primary data sources

  Federal providers:
  https://github.com/uscensusbureau/opportunity/edit/master/_data/data-providers/federal.yml
  see also: https://help.github.com/articles/rendering-csv-and-tsv-data/
** 311
   city services
   service requests
   submitted issues (Socrata:Detroit)
** code enforcements
   hearings
   inspections
   violations
   blight
   housing maintenance
   exterior violation cleanups
   licensure / inspection (biz)
   properties w/ ownership
   environmental violations (Balt.)?
** crime
   arrests
   public safety data sets
   major felony incidents
   historical crime
   calls for service/complaints (NOLA/Indy)?
   
** health + environment
   HIV/AIDS clinic
   women's health
   primary care
   hospitals
   community resiliency indicator system (SF)?
   health centers/facilities
   healthy corener store locations (Phil)?
   pharmacies
   child welfare indicators
   
** homelessness
   service facilities
   shelter locations
   point-in-time counts

** jobs
   job listings with location
   
** parcels and addresses
   neighborhood groups
   property addr. registry
   transportaion parcels
   street lane closures
   stormwater billing parcels?
   transportation parcels
   vacants
   waste mgmt (KCMO)
** permits
   business registry
   stree use
   building
   construction (+ pipeline (NY))
   new housing starts
   properties with permits obtained for work exceeding $50k (Balt)?
** privately owned assets
   supermarket locations
   restaurant inspections / scores
   real estate / land records
   churches
   hospitals
   rec centers
** publicly owned land
   parks
   recreation facilities
   public libraries
   vacant lot sales
   
** preschool / daycare
   childcare locations
   after-school programs
   licensed / registered providers
** schools
   point-locations
   surveys
   graduation rates
   test
   K-12
   private schools
   public schools
   after-school care
   crossing guards?
   post-secondary
   post-graduate
** transit
   MATA routes (stop/schedule)
   bikeway network (route/parking)
   commute
** wireless / broadband access
   HUB zones?
   wireless hotspots?
   
** zoning
   planning landuse and zoning
   tax credit zones?
   neighborhood groups?
   bonux program zoning districts?
   eligible parcels?
   zoning index?
   enterprise zones?
   base + overlay districts?
   primary com/res/ind
   school priority?
